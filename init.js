
(function test () { console.log("its working!") }) ();




class House {    //uppercase, creates House objects
    constructor(a, b) {
        this.color = a // will save to house object which is (this)
        this.texture = b
    }

    getFurniture() { // creates method
        return "1 couch"
    }

};

const houseObject = new House('red', 'rough'); // creates object called houseObject new calls the constructor and passes blue parameter into the houseObject.color object
const houseObject2 = new House('green')

console.log(houseObject.color) // calls houseObject and gets color value
console.log(houseObject.texture) // calls houseObject and gets color value
console.log(houseObject2.getFurniture()); // calls the function or method in the object2